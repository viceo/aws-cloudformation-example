#!/usr/bin/env sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR"/../.env
. "$DIR"/_01-validations.sh

# Referencia:
# https://docs.aws.amazon.com/es_es/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-package.html

sam package --debug \
  --template-file ./.aws-sam/build/template.yaml \
  --profile "$AWS_PROFILE" --region "$AWS_REGION" \
  --s3-bucket "$S3_SAM_BUCKET" \
  --s3-prefix "ejemplo-deploy" \
  --output-template-file deploy-template.yaml


if [ "$?" -eq "0" ]; then
  sam deploy \
    --stack-name "example-cloudformation" \
    --s3-bucket "$S3_SAM_BUCKET" \
    --profile "$AWS_PROFILE" --region "$AWS_REGION" \
    --capabilities CAPABILITY_NAMED_IAM \
    --template-file deploy-template.yaml \
    --parameter-overrides \
      StageEnvironment="$STAGE_ENVIRONMENT" \
      SrPagoAPICredentials="$SRPAGO_API_CREDENTIALS"
fi

BREAK_VALIDATION=false

if [ -z ${AWS_REGION+x} ]; then
  export AWS_REGION="us-east-1"
fi

if [ -z ${AWS_PROFILE+x} ]; then
  export AWS_PROFILE="default"
fi

if [ -z ${S3_SAM_BUCKET+x} ]; then
  echo "Requires S3_SAM_BUCKET definition"
  BREAK_VALIDATION=true
fi

# ---------------------------------------------------------
# region: Agrega tu validaciones necesarias para la existencia de las variables de entorno

if [ -z ${STAGE_ENVIRONMENT+x} ]; then
  echo "Required STAGE_ENVIRONMENT definition"
  BREAK_VALIDATION=true
fi


if [ -z ${SRPAGO_API_CREDENTIALS+x} ]; then
  echo "Required SRPAGO_API_CREDENTIALS definition"
  BREAK_VALIDATION=true
fi

# endregion: final de la validaciones
if [ "$BREAK_VALIDATION" = true ]; then
  exit 1
fi
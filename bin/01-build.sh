#!/usr/bin/env sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR"/../.env
. "$DIR"/_01-validations.sh

# Referencia
# https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-build.html

sam build $* \
  --profile "$AWS_PROFILE" \
  --use-container \
  --template "$DIR"/../template.yaml \
  --parameter-overrides \
    StageEnvironment="$STAGE_ENVIRONMENT" \
    SrPagoAPICredentials="$SRPAGO_API_CREDENTIALS"
#!/usr/bin/env sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR"/../.env
. "$DIR"/_01-validations.sh

# Referencia
# https://docs.aws.amazon.com/es_es/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-local-start-api.html

sam local start-api \
  -s "$DIR"/../public \
  -t "$DIR"/../.aws-sam/build/template.yaml \
  --parameter-overrides \
    StageEnvironment="$STAGE_ENVIRONMENT" \
    SrPagoAPICredentials="$SRPAGO_API_CREDENTIALS"
const axios = require('axios')
const AWS = require('aws-sdk')

// Create client outside of handler to reuse
const lambda = new AWS.Lambda()
const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 *
 */
exports.handler = async (event, context) => {
    console.log('## Environment variables:' + serialize(process.env));
    console.log('## CONTEXT: ' + serialize(context))
    console.log('## EVENT: ' + serialize(event))


    try {
        const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'hola a todos',
                location: ret.data.trim(),
                account: getAccountSettings()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }

    return response
};

const getAccountSettings = () => lambda.getAccountSettings().promise();

const serialize = (object) => JSON.stringify(object, null, 2);